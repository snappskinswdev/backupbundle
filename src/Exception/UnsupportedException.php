<?php

namespace Snappskin\BackupBundle\Exception;

class UnsupportedException extends \RuntimeException
{
}
